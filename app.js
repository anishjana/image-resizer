var express = require("express"),
	multer = require("multer"),
	sharp = require("sharp"),
	http = require('http'),
    app = express();
    
app.use(express.static("public"));

app.set("view engine", "ejs");


app.get("/", (req,res) => {
	res.render("index");
});


//configure multer
var storage = multer.diskStorage({
	destination: function (req, file, callb) {
		callb(null, 'public/uploads/');
	},
	filename: function(req, file, callb){
		
		callb(null, file.originalname);
	}
});

var upload = multer({storage: storage}).array("uploadImg");



//sharp function
app.post('/', upload, function (req, res){
	var file= req.files;
	let width = 600;
	let height = 600;
  file.forEach( function(element){
  		var x = element.path;
  		sharp(x)
		.resize(width,height, {
		kernel: sharp.kernel.nearest,
   		fit: 'contain',
    	position: 'center',
    	background: { r: 255, g: 255, b: 255, alpha: 1 }
	})
	.toFile('public/uploads/thumb/thumb_'+element.originalname, function(err){
		if(err){
			console.log(err)
		} 
	});
	
  });
	res.send("Image have been resized and stored in public/uploads/thumb");

});


app.listen(3000, () => {
  console.log(`Server running at port 3000`);
});